#!/bin/sh

while :
do
    SICK_CONT=$(docker ps --filter="health=unhealthy" --format "ID: {{.ID}}\tName: {{.Names}}\tStatus: {{.Status}}")
    SICK_CONT_COUNT=$(echo "$SICK_CONT" | wc -l)
    if [ "$SICK_CONT" != "" ];
    then
        echo $(date)
        echo "Achtung! $SICK_CONT_COUNT of your containers is unhealthy!"
        echo "====="
        echo "$SICK_CONT"
        echo "====="
    fi
    sleep 10
done
